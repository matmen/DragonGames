package listeners;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;

import main.DragonGames;

public class EntityPickupItem implements Listener {
	private DragonGames plugin = DragonGames.INSTANCE;

	@EventHandler
	public void onEntityPickupItem(EntityPickupItemEvent e) {
		if (e.getEntityType() != EntityType.PLAYER)
			return;

		if ((this.plugin.canMove) && (!this.plugin.dead.contains(((Player) e.getEntity()).getUniqueId()))) {
			e.setCancelled(false);
		} else {
			e.setCancelled(true);
		}
	}
}